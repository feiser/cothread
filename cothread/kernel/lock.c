/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 * 注意：所有互斥操作都是线程、中断之间的互斥。
 *       线程之间不需要互斥，因为线程之间不可抢占。
 *
 */

#include "rtos.h"

/* 非抢占调度器，用不上这两个锁操作 */
void mutex_lock(mutex_t *mutex)
{
    cothread_critical_stat();

    cothread_enter_critical();
    while (mutex->lock)
    {
        cothread_exit_critical();
        cothread_enter_critical();
    }
    mutex->lock = 1;
    cothread_exit_critical();
}

void mutex_unlock(mutex_t *mutex)
{
    mutex->lock = 0;
}

